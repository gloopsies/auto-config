#!/bin/sh

git clone https://git.radojevic.rs/petar/doas
cd doas
make install clean
cd ..

git clone https://git.radojevic.rs/petar/zsh-config
cd zsh-config
make doas install clean
cd ..

git clone https://git.radojevic.rs/petar/micro
cd micro
make build install clean
cd ..
